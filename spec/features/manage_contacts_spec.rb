require 'rails_helper'

feature 'contacts management' do
  context 'contact creating' do
    let(:contact_form) { ContactForm.new }
    scenario 'create contact with valid data' do
      contact_form.visit_page.fill_in_with(name: 'Mike Tyson').submit
      expect(Contact.last.name).to eq('Mike Tyson')
    end
    scenario 'display error when not valida data' do
      contact_form.visit_page.fill_in_with(name: nil).submit
      expect(page).to have_content("Name can't be blank")
    end
  end
  context 'contact deleting' do
    let(:contact) { create(:contact) }
    scenario 'delete contact' do
      visit contact_path(contact)
      click_link('Delete')
      expect(Contact.exists?(contact.id)).to be_falsey
      expect(current_path).to eq(contacts_path)
    end
  end
  context 'contact editing' do
    let(:contact) { create(:contact) }
    let(:contact_form) { ContactForm.new }
    scenario 'successfully edit contact name with valid data' do
      contact_form.visit_page(edit_contact_path(contact))
                  .fill_in_with(name: 'New Name').submit('Update Contact')
      contact.reload
      expect(contact.name).to eq('New Name')
    end
    scenario "can't set empty name for contact" do
      old_name = contact.name
      contact_form.visit_page(edit_contact_path(contact))
                  .fill_in_with(name: '').submit('Update Contact')
      contact.reload
      expect(contact.name).to eq(old_name)
      expect(page).to have_content("Name can't be blank")
    end
  end
end
