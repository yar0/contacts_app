# == Schema Information
#
# Table name: contacts
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'ffaker'

FactoryBot.define do
  factory :contact do
    sequence(:name) { FFaker::Name.unique.name }
  end
end
