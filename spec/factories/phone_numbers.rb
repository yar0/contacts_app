require 'ffaker'
# == Schema Information
#
# Table name: phone_numbers
#
#  id         :bigint(8)        not null, primary key
#  number     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :bigint(8)
#  tag_id     :bigint(8)
#
# Indexes
#
#  index_phone_numbers_on_contact_id  (contact_id)
#  index_phone_numbers_on_tag_id      (tag_id)
#
# Foreign Keys
#
#  fk_rails_...  (contact_id => contacts.id)
#  fk_rails_...  (tag_id => tags.id)
#

FactoryBot.define do
  factory :phone_number do
    number FFaker.numerify('+3###########')
    contact
    tag
  end
end
