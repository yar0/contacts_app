require 'rails_helper'

RSpec.describe ContactsController, type: :controller do

  describe 'GET #index' do
    let(:contacts_list) { create_list(:contact, 3) }
    it 'responds with :OK' do
      get :index
      expect(response).to have_http_status(:ok)
    end
    it 'renders :index template' do
      get :index
      expect(response).to render_template(:index)
    end
    it 'assigns all contacts to @contacts' do
      get :index
      expect(assigns[:contacts]).to eq(contacts_list)
    end
  end
  describe 'GET #new' do
    it 'responds with :OK' do
      get :new
      expect(response).to have_http_status(:ok)
    end
    it 'renders :new template' do
      get :new
      expect(controller).to render_template(:new)
    end
    it 'assigns new Contact to @contact' do
      get :new
      expect(assigns[:contact]).to be_a_new(Contact)
    end
  end
  describe 'POST #create' do
    let(:contact_params) { attributes_for(:contact) }
    it 'save new contact to the database' do
      expect {
        post :create, params: { contact: contact_params }
      }.to change(Contact, :count).by(1)
    end
    it 'redirects to contact :show action' do
      post :create, params: { contact: contact_params }
      expect(controller).to redirect_to(contact_path(Contact.last.id))
    end
  end
  describe 'DELETE #destroy' do
    let(:contact) { FactoryBot.create(:contact) }
    it 'responds with destroyed contact id when requested by remote link' do
      request.headers['Content-Type'] = 'application/json'
      request.headers['Accept'] = 'text/javascript, application/javascript,
       application/ecmascript, application/x-ecmascript, */*; q=0.01'

      delete :destroy, params: { id: contact.id }, xhr: true
      json_response = JSON.parse(response.body).symbolize_keys
      expect(json_response).to eq(contact_id: contact.id)
    end

    it 'redirects to contacts when request format is HTML' do
      delete :destroy, params: { id: contact.id }
      expect(controller).to redirect_to(contacts_path)
    end
  end
  describe 'GET #show' do
    let(:contact) { create(:contact) }
    it 'responds with :ok' do
      get :show, params: { id: contact.id }
      expect(response).to have_http_status(:ok)
    end
    it 'renders :show template' do
      get :show, params: { id: contact.id }
      expect(response).to render_template(:show)
    end
    it 'assigns contact to @contact' do
      get :show, params: { id: contact.id }
      expect(assigns[:contact]).to eq(contact)
    end
  end
  describe 'GET #edit' do
    let(:contact) { create(:contact) }
    it 'renders :edit template' do
      get :edit, params: { id: contact.id }
      expect(controller).to render_template(:edit)
    end
    it 'assign contact to @contact' do
      get :edit, params: { id: contact.id }
      expect(assigns[:contact]).to eq(contact)
    end
  end
  describe 'PUT #update' do
    let(:contact) { create(:contact) }
    it 'redirects to contact if updated' do
      put :update, params: { id: contact.id, contact: { name: 'New name' } }
      expect(controller).to redirect_to(contact_path(contact))
    end
    it 'render :edit if not valid params' do
      put :update, params: { id: contact.id, contact: { name: nil } }
      expect(response).to render_template(:edit)
    end
  end
end
