require 'rails_helper'

RSpec.describe Contact, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_length_of(:name).is_at_most(200) }
  it { should have_many(:phone_numbers) }

  describe '::search' do
    it 'returns contacts by name or part of name' do
      tyson = create(:contact, name: 'Mike Tyson')
      c1 = create(:contact, name: 'Andrew Racer')
      c2 = create(:contact, name: 'Andrew Tracer')

      expect(Contact.search('racer')).to include(c2, c1)
      expect(Contact.search('racer')).to_not include(tyson)
    end
    it 'returns contacts by phone number or part of number' do
      c1 = create(:contact)
      c2 = create(:contact)
      create(:phone_number, contact: c1, number: '+380999999999')
      create(:phone_number, contact: c1, number: '+380955999999')
      create(:phone_number, contact: c2, number: '+380999777999')
      expect(Contact.search('777')).to include(c2)
    end
  end
end
