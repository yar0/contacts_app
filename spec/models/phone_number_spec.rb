require 'rails_helper'

RSpec.describe PhoneNumber, type: :model do
  it { should validate_presence_of(:number) }
  it { should belong_to(:contact) }
  it { should belong_to(:tag) }
  let(:phone) { create(:phone_number) }
  it 'is not valid with incorrect number format' do
    phone.number = '+4asdassdasdsad'
    expect(phone).to_not be_valid
  end
  it 'is valid with correct number format' do
    phone.number = '+480998090344'
    expect(phone).to be_valid
  end
end
