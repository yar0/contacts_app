class ContactForm
  include Capybara::DSL

  def visit_page(page = '/contacts/new')
    visit(page)
    self
  end

  def fill_in_with(params = {})
    fill_in('Name', with: params.fetch(:name, 'Default Contact'))
    self
  end

  def submit(button_name = 'Create Contact')
    click_on(button_name)
  end
end