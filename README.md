# README

'Телефонна книга' 

Вимоги:

* До контакту може бути прив’язано кілька телефонних номерів з можливістю присвоїти кожному номеру один зі стандартних ярликів (мобільний, домашній, робочий) або введений вручну.

* Телефонний номер повинен відображатись та зберігатись у міжнародному форматі.

* Пошук по номеру, частині номера, назві чи частині назви контакту. Усе з одного поля пошуку, з миттєвим оновленням результатів.

Залежності:

* ruby 2.5.*
* rails ~> 5.1.6
* Postgresql 9.*

Запуск:
1. git clone https://yar0@bitbucket.org/yar0/contacts_app.git
2. cd contacts_app
3. rake db:create
4. rake db:migrate
5. rake db:seed
6. rails s
7. [open app](http://localhost:3000/)

TODO: покрити тестами решту функціоналу.

