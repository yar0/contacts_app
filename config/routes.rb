Rails.application.routes.draw do
  root to: 'contacts#index'
  resources :contacts do
    resources :phone_numbers
  end
  resource :tags, only: [:create]
  get '/search', to: 'search#search'
end
