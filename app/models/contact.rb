# == Schema Information
#
# Table name: contacts
#
#  id         :bigint(8)        not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Contact < ApplicationRecord
  validates :name, presence: true, length: { maximum: 200 }
  has_many :phone_numbers, -> { includes :tag }, dependent: :delete_all

  def self.search(query)
    left_outer_joins(:phone_numbers)
        .where('contacts.name ILIKE :query OR phone_numbers.number ILIKE :query', query: "%#{query}%").distinct
  end
end
