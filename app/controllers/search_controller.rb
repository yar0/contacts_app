class SearchController < ApplicationController
  def search
    render json: Contact.search(params[:query]).pluck(:id).map(&:to_s)
  end
end
