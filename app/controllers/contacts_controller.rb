class ContactsController < ApplicationController

  def index
    @contacts = Contact.all
  end

  def show
    @contact = Contact.find(params[:id])
    @phone_number = PhoneNumber.new(contact: @contact)
    @select_tags = Tag.pluck(:name, :id) << ['Create custom', -1]
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      redirect_to @contact, notice: 'Contact successfully created!'
    else
      render :new
    end
  end

  def edit
    @contact = Contact.find(params[:id])
  end

  def update
    @contact = Contact.find(params[:id])
    if @contact.update_attributes(contact_params)
      redirect_to @contact, notice: 'Contact successfully updated!'
    else
      render :edit
    end
  end

  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy

    respond_to do |format|
      format.json { render json: { contact_id: @contact.id } }
      format.html { redirect_to contacts_path }
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name)
  end
end
