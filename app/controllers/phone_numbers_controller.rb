class PhoneNumbersController < ApplicationController

  def destroy
    @phone_number = PhoneNumber.find(params[:id])
    @phone_number.destroy
    render json: { phone_id: @phone_number.id }
  end

  def create
    @phone_number = PhoneNumber.new(phone_number_params)
    @phone_number.contact_id = params[:contact_id]
    if @phone_number.save
      phone_html = render_to_string(template: 'phone_numbers/_phone_number',
                                    layout: false,
                                    locals: { phone_number: @phone_number })
      render json: { phone: phone_html }
    else
      error_msg = 'Phone number ' + @phone_number.errors.messages[:number].first
      flash.now[:notice] = error_msg
      render json: { error: error_msg }, status: :bad_request
    end
  end

  private

  def phone_number_params
    params.require(:phone_number).permit(:number, :tag_id)
  end
end
