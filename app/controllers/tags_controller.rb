class TagsController < ApplicationController

  def create
    @tag = Tag.new(tag_params)
    @tag.predefined = false
    render json: { tag: @tag } if @tag.save
  end

  private

  def tag_params
    params.require(:tag).permit(:name)
  end
end
