# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
handleTagCreation = (event)->
  tag = event.detail[0].tag
  $("#phone_number_tag_id option:last").before($("<option></option>")
    .attr("value", tag.id)
    .text(tag.name))
  $("#tag_modal_form").modal('hide')
  $("#phone_number_tag_id").val(tag.id)
  $("tag_name").val("")

handleTagCreationError = (e) ->
  console.log('Tag creation error.')

$(document).on "turbolinks:load", ->
  $("#new_tag").on("ajax:success",
    handleTagCreation
  ).on("ajax:error", handleTagCreationError)

  $("#new_phone_number").on("submit", (event)->
    if $("#phone_number_tag_id").val() == '-1'
      alert("Please pick phone tag.")
      event.preventDefault()
      return false
  )