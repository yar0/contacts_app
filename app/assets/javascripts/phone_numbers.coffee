# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

displayTagModalForm = ()->
  $("#tag_modal_form").modal();

handleCreatePhoneNumber = (event)->
  $('#phones').append(event.detail[0].phone)
  $('#phone_number_number').removeClass('is-invalid')
  $('#phone_number_number').val('')

handleCreatePhoneNumberError = (event)->
  data = event.detail[0]
  console.log(data)
  $('#phone_number_number').addClass('is-invalid')
  alert(data.error)

$(document).on "turbolinks:load", ->
  $("select#phone_number_tag_id").on("change", (event) ->
    console.log(this.value)
    if this.value == '-1'
      displayTagModalForm()
  )

  $("#new_phone_number").on("ajax:success",
    handleCreatePhoneNumber
  ).on("ajax:error",
    handleCreatePhoneNumberError
  )

  $("#phones").on("ajax:success", "a.delete_phone", (event) ->
    $("#phone_" + event.detail[0].phone_id).remove()
  )