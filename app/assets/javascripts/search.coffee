# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

displayFoudContacts = (foundIds)->
  $('.searchable').each((i, contact)->
    contactId = contact.dataset.contactId
    if contactId in foundIds
      $(contact).show()
    else
      $(contact).hide()
  )

$(document).ready ->
  $("#search_query").on("input", (event) ->
    $.ajax(url: "/search?query=" + this.value).done (foundIds)->
      displayFoudContacts(foundIds)
  )