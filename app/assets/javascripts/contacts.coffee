# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

handleContactDelete = (event) ->
  contactId = event.detail[0].contact_id
  $("#contact_" + contactId).remove()

handleContactDeletingError = (event) ->
  console.log("Can't remove contact.")

$(document).on "turbolinks:load", ->
  $(".delete_contact").on("ajax:success",
    handleContactDelete
  ).on("ajax:error", handleContactDeletingError)